from django.urls import path

from receipts.views import ReceiptListView


urlpatterns = [
    path("receipts/", ReceiptListView.as_view(), name="home"),
]

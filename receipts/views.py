from django.shortcuts import render
from django.views.generic import ListView
from receipts.models import Receipt


# Create your views here.


class ReceiptListView(ListView):
    model = Receipt
    template_name = "receipt/list.html"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)

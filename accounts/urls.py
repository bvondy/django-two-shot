from django.urls import path

from accounts.views import LoginView, LogoutView, sign_up


urlpatterns = [
    path("login/", LoginView.as_view(), name="login"),
    path("logout/", LogoutView.as_view(), name="logout"),
    path("signup/", sign_up, name="signup"),
]
